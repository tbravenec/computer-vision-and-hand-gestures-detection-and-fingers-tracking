'''Module with function for hand keypoints detection'''
import cv2
import numpy as np
import torch

import constants
from enums.hands import Hands


def detect(model, img, use_cuda=True, half_precision=False):
    '''Detect keypoints in image'''
    aspect_ratio = img.shape[1] / img.shape[0]

    threshold = 0.1

    net_width = int(aspect_ratio * constants.KEYPOINTS_NET_HEIGHT)

    net_input = cv2.resize(img, (net_width, constants.KEYPOINTS_NET_HEIGHT))

    if half_precision:
        net_input = np.float16(np.moveaxis(net_input, 2, 0)) / 255
    else:
        net_input = np.float32(np.moveaxis(net_input, 2, 0)) / 255

    net_input = np.expand_dims(net_input, 0)
    net_input = torch.from_numpy(net_input)

    if use_cuda:
        net_input = net_input.cuda()

    output = model.forward(net_input)

    if use_cuda:
        output = output.cpu().data.numpy()
    else:
        output = output.data.numpy()

    keypoints = list()

    for i in range(constants.HAND_KEYPOINTS):
        probability_map = output[0, i, :, :]

        if half_precision:
            probability_map = np.float32(probability_map)

        probability_map = cv2.resize(probability_map, (img.shape[1], img.shape[0]))

        _, max_val, _, max_loc = cv2.minMaxLoc(probability_map)

        if max_val > threshold:
            keypoints.append((int(max_loc[0]), int(max_loc[1])))
        else:
            keypoints.append(None)

    return keypoints, Hands.unknown


def detect_right_left(model, img, use_cuda=True, half_precision=False):
    '''Detect keypoints in image'''
    aspect_ratio = img.shape[1] * 2 / img.shape[0]

    threshold = 0.1

    net_width = int(aspect_ratio * constants.KEYPOINTS_NET_HEIGHT)

    img_input = np.hstack((img, cv2.flip(img, 1)))

    net_input = cv2.resize(img_input, (net_width, constants.KEYPOINTS_NET_HEIGHT))

    if half_precision:
        net_input = np.float16(np.moveaxis(net_input, 2, 0)) / 255
    else:
        net_input = np.float32(np.moveaxis(net_input, 2, 0)) / 255

    net_input = np.expand_dims(net_input, 0)
    net_input = torch.from_numpy(net_input)

    if use_cuda:
        net_input = net_input.cuda()

    output = model.forward(net_input)

    if use_cuda:
        output = output.cpu().data.numpy()
    else:
        output = output.data.numpy()

    keypoints_left = list()
    keypoints_right = list()
    max_vals_right = list()
    max_vals_left = list()

    for i in range(constants.HAND_KEYPOINTS):
        probability_map = output[0, i, :, :]

        if half_precision:
            probability_map = np.float32(probability_map)

        probability_map = cv2.resize(probability_map, (img.shape[1] * 2, img.shape[0]))

        probability_map_split = np.hsplit(probability_map, 2)

        # Keypoint selection for right hand
        probability_map_right = probability_map_split[0]

        _, max_val_right, _, max_loc_right = cv2.minMaxLoc(probability_map_right)

        max_vals_right.append(max_val_right)

        if max_val_right > threshold:
            keypoints_right.append((int(max_loc_right[0]), int(max_loc_right[1])))
        else:
            keypoints_right.append(None)

        # Keypoint selection for left hand
        probability_map_left = probability_map_split[1]

        _, max_val_left, _, max_loc_left = cv2.minMaxLoc(probability_map_left)

        max_vals_left.append(max_val_left)

        if max_val_left > threshold:
            keypoints_left.append((int(img.shape[1] - max_loc_left[0]), int(max_loc_left[1])))
        else:
            keypoints_left.append(None)

    # Decision if left or right hand
    rights = lefts = 0

    for right, left in zip(max_vals_right, max_vals_left):
        if right >= left:
            rights += 1
        else:
            lefts += 1

    return (keypoints_right, Hands.right) if rights >= lefts else (keypoints_left, Hands.left)


def draw_skeleton(img, keypoints, points=True):
    '''Draw hand skeleton into image from detected keypoints'''
    color = -1
    for i, pair in enumerate(constants.HAND_POSE_PAIRS):
        part_a = pair[0]
        part_b = pair[1]

        if i % 4 == 0:
            color = color + 1

        if keypoints[part_a] and keypoints[part_b]:
            if part_a in (0, 1):
                cv2.line(img, keypoints[part_a], keypoints[part_b], constants.COLORS[5], 2)
            else:
                cv2.line(img, keypoints[part_a], keypoints[part_b], constants.COLORS[color], 2)

            if points:
                cv2.circle(img, keypoints[part_a], 4, (0, 0, 0), thickness=-1, lineType=cv2.FILLED)
                cv2.circle(img, keypoints[part_b], 4, (0, 0, 0), thickness=-1, lineType=cv2.FILLED)

    return img


def keypoints_offset(keypoints, offset_x, offset_y):
    '''Offset keypoints in pixels'''
    updated = list()
    for keypoint in keypoints:
        if keypoint is not None:
            updated.append((keypoint[0] + offset_x, keypoint[1] + offset_y))
        else:
            updated.append(None)

    return updated


def get_hand_offset(img, box):
    '''Get offset of bounding box in pixels'''
    x = int(((box[0] - box[2]/2.0) * img.shape[1]))
    y = int(((box[1] - box[3]/2.0) * img.shape[0]))

    return x, y
