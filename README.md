# Computer vision and hand gestures detection and fingers tracking

Master's thesis project for Brno University of Technology, focused on hand gesture detection and finger tracking.
The full thesis can be downloaded on the VUT website from [here][thesis]

## Instructions

The application is multiplatform, and can be run on either Microsoft Windows or Linux based operating systems.
The instructions differ slightly for each system.

### Software requirements
Only requirements to run this application are the system memory, which is recommended to be at least 8 GB

Optionally should the system be equipped with NVIDIA GPU with at least 2 GB of memory for partial functionality
or 6+ GB of video memory for complete functionality.

By default the PyTorch installed is compiled against CUDA 10, which means that to utilize the 
computational power of the GPU, the video driver must be of version 410 or higher. For other versions of CUDA
the list of required driver versions can be found in [NVIDIA documentation][driver].


### Microsoft Windows
1) Download or clone this repository
2) Download and install Python 3.6 from [here][python_win] or 
directly download installer from [here][python_win_direct]
    - Make sure that "Add python to Path" button is checked during installation
    - In case you already have python installed you can check the version with command:

        ~~~~
        python --version
        ~~~~
    - If the python version installed is higher than 3.6, the link to PyTorch in the 
    requirements.txt has to be changed according to the [PyTorch GetStarted Guide][PTguide] with this settings:
        - PyTorch Build: Stable
        - Your OS: Windows
        - Package: Pip
        - Language: "Your python version"
            - Normally Python 3.6 is used
        - CUDA: "CUDA version supported by your GPU or None for CPU only"
            - Normally CUDA 10.0 is used
    - This settings will generate command for manual installation. Part of this command contains a 
    link to the whl file, you should replace the link on line 8 of the requirements.txt with this new link.
    
3) Open command line and navigate to the root of the downloaded repository and run command:

    ~~~~
    python -m pip install -r requirements.txt
    ~~~~    

    - This command will install all of the python packages required by the application


4) Launch the main_app.py either by double click (if python is correctly set as startup program for .py files) 
    or from command line with:

    ~~~~
    python main_app.py
    ~~~~

    - This command will launch the application

During the first time launch, the application will take couple of minutes (depending on the network speed)
to download the weights for both neural networks.

### Linux
Linux based systems usually have python already installed, unfortunatelly this does not make the installation easier.

1) Download or clone this repository
2) Install Kivy (GUI framework) dependencies with series of commands:
    - These commands work on Ubuntu based distributions, for other Linux distributions follow the
    complete [Kivy Installation Guide][kivy], from which the following commands are taken.
    - Add the kivy repository

        ~~~~
        sudo add-apt-repository ppa:kivy-team/kivy
        ~~~~
    - Install kivy:

        ~~~~
        sudo apt-get install python3-kivy
        ~~~~
    - Install all kivy dependencies, including python, if it is not installed:

        ~~~~
        sudo apt-get install -y \
            python3-pip \
            build-essential \
            git \
            python3 \
            python3-dev \
            ffmpeg \
            libsdl2-dev \
            libsdl2-image-dev \
            libsdl2-mixer-dev \
            libsdl2-ttf-dev \
            libportmidi-dev \
            libswscale-dev \
            libavformat-dev \
            libavcodec-dev \
            zlib1g-dev
        ~~~~
3) Check the python version with command:
    
    ~~~~
    python3 --version
    ~~~~
    - If the python version installed is higher than 3.6, the link to PyTorch in the 
    requirements.txt has to be changed according to the [PyTorch GetStarted Guide][PTguide] with this settings:
        - PyTorch Build: Stable
        - Your OS: Linux
        - Package: Pip
        - Language: "Your python version"
            - Normally Python 3.6 is used
        - CUDA: "CUDA version supported by your GPU or None for CPU only"
            - Normally CUDA 10.0 is used
    - This settings will generate command for manual installation. Part of this command contains a 
    link to the whl file, you should replace the link on line 9 of the requirements.txt with this new link.
    - If CUDA 9 is to be used, the link can be just replaced by word "torch" 
4) Open command line and navigate to the root of the downloaded repository and run command:

    ~~~~
    pip3 install -r requirements.txt
    ~~~~    

    - This command will install all of the python packages required by the application
    - If you want, you can also create virtual enviroment and install all packages there to keep the system
    installation of python clean


5) Launch the main_app.py from terminal with command:

    ~~~~
    python3 main_app.py
    ~~~~

    - This command will launch the application

## Citation
Please cite this paper in your publications if it helps your research:
~~~~
@MastersThesis{BravenecMasters2019,
  author = {Tomas Bravenec},
  title = {Computer Visiond and Hand Gestures Detection and Fingers Tracking},
  school = {Brno University of Technology},
  address = {Brno},
  year = {2019},
}
~~~~
## License and Third party code
- The application uses Darknet framework interpreter for PyTorch, that allows to use original Darknet
configuration files and weights without any conversion. The original repository can be found [here][darknet],
the interpreter is published under MIT license, which can be found in the repository
- The hand key point detector is part of the project OpenPose, which is free for non commercial use.
The OpenPose repository can be found [here][OpenPose], from the project OpenPose the hand key point
detector is used, and can be used only for non commercial and research purposes, detailed information
can be found in the LICENSE of the project OpenPose

[thesis]: https://www.vutbr.cz/studenti/zav-prace/detail/118437 "Thesis"
[python_win]: https://www.python.org/downloads/release/python-366/ "Python 3.6"
[python_win_direct]: https://www.python.org/ftp/python/3.6.6/python-3.6.6-amd64.exe "Python 3.6 installer"
[kivy]: https://kivy.org/doc/stable/installation/installation-linux.html#using-software-packages "Installation on Linux"
[PTguide]: https://pytorch.org/get-started/locally/ "PyTorch Get Started"
[driver]: https://docs.nvidia.com/deploy/cuda-compatibility/index.html#binary-compatibility__table-toolkit-driver "Driver Versions"
[darknet]: https://github.com/andy-yun/pytorch-0.4-yolov3 "pytorch-0.4-yolov3 : Yet Another Implimentation of Pytroch 0.41 or over and YoloV3"
[OpenPose]: https://github.com/CMU-Perceptual-Computing-Lab/openpose "OpenPose"