'''Functions for models download'''

import os
import sys

import wget

def get_models(directory=''):
    '''Downloads both models into directory'''
    get_detection_model(directory)
    get_keypoint_model(directory)

    return 0

def get_detection_model(directory=None, filename='yolov2.weights'):
    '''Downloads hand detection model into directory'''

    if directory is not None:    
        if not os.path.exists(directory):
            os.makedirs(directory)

        path = os.path.join(directory, filename)
    else:
        path = filename

    if os.path.exists(path):
        return path

    url = 'https://gitlab.com/tbravenec/assets/raw/master/hand_detection/yolov2.weights'

    return wget.download(url, path)

def get_keypoint_model(directory=None, filename='keypoints.pth'):
    '''Downloads keypoints detection model into directory'''
    if directory is not None:    
        if not os.path.exists(directory):
            os.makedirs(directory)

        path = os.path.join(directory, filename)
    else:
        path = filename

    if os.path.exists(path):
        return path

    url = 'https://gitlab.com/tbravenec/assets/raw/master/hand_keypoint_detection/keypoints.pth'

    return wget.download(url, path)

if __name__ == "__main__":
    folder = 'weights'

    print(get_detection_model())
    print(get_keypoint_model(folder))

    sys.exit(0)
