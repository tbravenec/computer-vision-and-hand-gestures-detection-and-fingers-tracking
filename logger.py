'''Modul with the Logger class'''

class Logger():
    '''Class used for creating and saving logs'''

    file = None
    log = list()
    header = False

    def __init__(self, filename=None):
        if filename is not None:
            self.file = open(filename, 'w')

    def __del__(self):
        if self.file is not None:
            self.file.writelines(map(lambda s: s + '\n', self.log))
            self.file.close()
        self.clear_log()

    def add_header(self, header):
        '''Adds or replaces header if it exists'''
        if self.header:
            self.log.pop(0)

        self.log.insert(0, header)
        self.header = True

    def add_line(self, line, index=None):
        '''Appends the line to the log'''
        if index is None:
            self.log.append(line)
        else:
            self.log.insert(index, line)

    def add_record(self, index, single_occurence, *args):
        '''Automatically includes all and sorts them by index'''
        line = ' '.join((str(index), ' '.join(map(str, args))))

        if line not in self.log:
            for id, record in enumerate(self.log):
                if self.header and id == 0:
                    continue

                if index == int(record.split(' ')[0]) and single_occurence:
                    self.log.pop(id)
                    self.log.insert(id, line)
                    break
                if index == int(record.split(' ')[0]) and not single_occurence:
                    self.log.insert(id + 1, line)
                    break
                elif index < int(record.split(' ')[0]):
                    self.log.insert(id, line)
                    break
            else:
                self.log.append(line)

    def clear_log(self):
        '''Removes all log entries'''
        self.log.clear()
        self.header = False

    def change_filename(self, filename):
        '''Changes log output destination'''
        if self.file is not None:
            self.file.close()

        self.file = open(filename, 'w')

    def save_log(self, filename):
        '''Save log into file'''
        if self.file is None:
            self.file = open(filename, 'w')

        self.file.writelines(map(lambda s: s + '\n', self.log))
        self.file.close()
        self.file = None