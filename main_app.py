'''Main Computer Vision and Hand Gestures Detection and Finger Tracking file'''
import os
from threading import Thread
from time import sleep, time

import cv2
from kivy.app import App
from kivy.animation import Animation
from kivy.clock import Clock
from kivy.properties import BooleanProperty, ListProperty, NumericProperty
from kivy.properties import Property, StringProperty

import constants
from enums.input_types import InputTypes
from get_models import get_detection_model, get_keypoint_model
from hand_tracking import HandTracking
from logger import Logger
from utils import check_cuda, cv_image_to_kivy_texture, get_timestamp

class MainApp(App):
    '''Main application class based on Kivy's App'''
    show_settings = BooleanProperty(False)
    show_controls = BooleanProperty(False)
    show_record = BooleanProperty(False)
    show_record_settings = BooleanProperty(False)
    show_advanced_record_settings = BooleanProperty(False)
    show_info = BooleanProperty(False)
    show_save_image_button = BooleanProperty(False)
    play = BooleanProperty(False)
    stop_threads = BooleanProperty(False)
    next_frame = BooleanProperty(False)
    previous_frame = BooleanProperty(False)
    recording_state = BooleanProperty(False)
    recording_pause = BooleanProperty(False)
    output_frames = ListProperty([])
    frame_index = NumericProperty(0)
    frame_target = NumericProperty(0)
    total_frames = NumericProperty(0)
    input_fps = NumericProperty(0)
    input_filename = StringProperty('')
    skip_frames = NumericProperty(0)
    input_type = InputTypes.none
    output_path = StringProperty('')
    output_fourcc = StringProperty('MJPG')
    output_fps = NumericProperty(0)
    output_width = NumericProperty(0)
    output_height = NumericProperty(0)
    save_record_log = BooleanProperty(True)
    save_log = BooleanProperty(False)
    logger = None
    record_logger = None
    popup_text = StringProperty('Popup text')
    popup_title = StringProperty('Popup title')
    popup_button_text = StringProperty('Popup button text')
    video_thread = None
    last_frame = None
    hand_tracker = None
    capture = Property(None)
    writer = None

    def build(self):
        self.title = 'Computer vision and hand gestures detection and fingers tracking'
        self.icon = 'data/icons/app.ico'
        Clock.schedule_interval(self.update_image, 0)
        return super().build()

    def on_start(self):
        '''Event callback to run on application start up'''
        cfgfile_detector = 'cfg/yolov2.cfg'
        weights_detector = get_detection_model('weights')
        cfg_keypoints = 'cfg/keypoints.py'
        weights_keypoints = get_keypoint_model('weights')

        self.hand_tracker = HandTracking(cfgfile_detector, None, weights_detector,
                                         cfg_keypoints, weights_keypoints)

        self.hand_tracker.load_gestures('gestures')

        self.root.ids.cuda_switch.disabled = not check_cuda()
        self.root.ids.cuda_switch.active = check_cuda()
        self.root_window.minimum_width = 800
        self.root_window.minimum_height = 600

    def on_stop(self):
        '''Event callback to run on application exit'''
        if self.input_type == InputTypes.video and self.save_log:
            header = '{}, FPS: {}, Shape: {}x{}'.format(self.input_filename, self.input_fps,
                                                        self.last_frame.shape[1],
                                                        self.last_frame.shape[0])
            self.logger.add_header(header)
            self.logger.save_log(os.path.splitext(self.input_filename)[0] + '_log.txt')

        self.video_thread_stop()

    def start_camera(self):
        '''Callback from UI to start hand tracking from camera'''
        if self.input_type == InputTypes.video and self.logger.log and self.save_log:
            header = '{}, FPS: {}, Shape: {}x{}'.format(self.input_filename,
                                                        self.input_fps,
                                                        self.last_frame.shape[1],
                                                        self.last_frame.shape[0])
            self.logger.add_header(header)
            self.logger.save_log(os.path.splitext(self.input_filename)[0] + '_log.txt')

        self.input_filename = ''
        self.show_save_image_button = False
        self.capture = cv2.VideoCapture(0)
        self.frame_target = int(self.capture.get(cv2.CAP_PROP_FPS))
        self.input_type = InputTypes.camera
        self.total_frames = int(self.capture.get(cv2.CAP_PROP_FRAME_COUNT))
        self.input_fps = int(self.capture.get(cv2.CAP_PROP_FPS))

        if self.output_path == '':
            self.output_path = self.selected_save_file('', '/')

        self.output_fps = int(self.capture.get(cv2.CAP_PROP_FPS))
        self.output_width = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.output_height = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))

        self.root.ids.play_button.disabled = False
        self.root.ids.next_button.disabled = False
        self.root.ids.previous_button.disabled = True
        self.root.ids.slider.disabled = True

        if not self.play:
            self.toggle_play_pause()

        self.video_thread_stop()
        self.video_thread = Thread(target=self.video_feed)
        self.video_thread.start()
        
        self.frame_index = 0

        if not self.show_controls:
            self.toggle_controls()

    def selected_file(self, filename):
        '''Callback from UI ran after file selection'''
        self.show_save_image_button = False

        if self.input_type == InputTypes.video and self.logger.log and self.save_log:
            header = '{}, FPS: {}, Shape: {}x{}'.format(self.input_filename,
                                                        self.input_fps,
                                                        self.last_frame.shape[1],
                                                        self.last_frame.shape[0])
            self.logger.add_header(header)
            self.logger.save_log(os.path.splitext(self.input_filename)[0] + '_log.txt')

        self.input_filename = filename
        if os.path.exists(filename):
            if os.path.splitext(filename)[1].lower() in constants.IMAGE_FORMATS:
                input_image = cv2.imread(filename)
                self.logger = Logger()
                self.input_type = InputTypes.image
                self.total_frames = 0
                self.input_fps = 0

                self.root.ids.play_button.disabled = True
                self.root.ids.next_button.disabled = True
                self.root.ids.previous_button.disabled = True
                self.root.ids.snap_button.disabled = True
                self.root.ids.slider.disabled = True

                self.video_thread_stop()
                Thread(target=self.single_image, args=[input_image]).start()

                if self.show_controls:
                    self.toggle_controls()

            elif os.path.splitext(filename)[1].lower() in constants.VIDEO_FORMATS:
                self.capture = cv2.VideoCapture(filename)
                self.frame_target = int(self.capture.get(cv2.CAP_PROP_FPS))
                self.logger = Logger()
                self.input_type = InputTypes.video
                self.total_frames = int(self.capture.get(cv2.CAP_PROP_FRAME_COUNT))
                self.input_fps = int(self.capture.get(cv2.CAP_PROP_FPS))

                if self.output_path == '':
                    self.output_path = self.selected_save_file('', '/')

                self.output_fps = int(self.capture.get(cv2.CAP_PROP_FPS))
                self.output_width = int(self.capture.get(cv2.CAP_PROP_FRAME_WIDTH))
                self.output_height = int(self.capture.get(cv2.CAP_PROP_FRAME_HEIGHT))

                self.root.ids.play_button.disabled = False
                self.root.ids.snap_button.disabled = True
                self.root.ids.slider.disabled = False
                self.slider_set_min_max(self.capture.get(cv2.CAP_PROP_FRAME_COUNT))

                if self.play:
                    self.toggle_play_pause()

                self.video_thread_stop()
                self.video_thread = Thread(target=self.video_feed)
                self.video_thread.start()
                
                self.frame_index = 0

                if not self.show_controls:
                    self.toggle_controls()

        self.root.ids.open_popup.dismiss()

    def selected_save_file(self, filename, path):
        '''Save iamge into file or set video path'''
        if path == '/':
            path = os.path.abspath(path)

        if os.path.isdir(filename):
            path = filename
            filename = ''

        if filename.strip() == '':
            if self.input_type in (InputTypes.camera, InputTypes.video):
                filename = 'Output_Video' + constants.VIDEO_FORMATS[0]
            elif self.input_type == InputTypes.image:
                filename = 'Output_Image' + constants.IMAGE_FORMATS[0]

        if self.input_type in (InputTypes.camera, InputTypes.video):
            if not os.path.splitext(filename)[1].lower() in constants.VIDEO_FORMATS:
                filename = os.path.splitext(filename)[0] + constants.VIDEO_FORMATS[0]
        elif self.input_type == InputTypes.image:
            if not os.path.splitext(filename)[1].lower() in constants.IMAGE_FORMATS:
                filename = os.path.splitext(filename)[0] + constants.IMAGE_FORMATS[0]

        self.root.ids.save_popup.dismiss()

        filepath = os.path.join(path, filename)

        if self.input_type in (InputTypes.camera, InputTypes.video):
            self.output_path = filepath
        elif self.input_type == InputTypes.image and self.last_frame is not None:
            cv2.imwrite(filepath, self.last_frame)
            if self.logger.log and self.save_log:
                header = '{}, Shape: {}x{}'.format(filepath, self.last_frame.shape[1],
                                                   self.last_frame.shape[0])
                self.logger.add_header(header)
                self.logger.save_log(os.path.splitext(filepath)[0] + '_log.txt')

        return filepath

    def toggle_settings(self):
        '''Callback from UI to toggle settings visibility'''
        self.show_settings = not self.show_settings
        if self.show_settings:
            height = 250
        else:
            height = 0

        Animation(height=height, d=0.3, t='out_quart').start(self.root.ids.settings)

    def toggle_controls(self):
        '''Callback from UI to toggle controls visibility'''
        self.show_controls = not self.show_controls
        if self.show_controls:
            height = 40
        else:
            height = 0

        Animation(height=height, d=0.3, t='out_quart').start(self.root.ids.controls)

    def toggle_record_panel(self):
        '''Callback from UI to toggle record panel visibility'''
        self.show_record = not self.show_record
        if self.show_record:
            width = 50
        else:
            width = 0

        Animation(width=width, d=0.3, t='out_quart').start(self.root.ids.record_panel)

    def toggle_record_settings(self):
        '''Callback from UI to toggle record settings panel visibility'''
        self.show_record_settings = not self.show_record_settings
        if self.show_record_settings:
            width = self.root.width * 0.3
        else:
            width = 0

        Animation(width=width, d=0.3, t='out_quart').start(self.root.ids.record_settings_panel)

    def toggle_advanced_record_settings(self):
        '''Callback from UI to toggle record settings panel visibility'''
        self.show_advanced_record_settings = not self.show_advanced_record_settings
        if self.show_advanced_record_settings:
            height = 250
        else:
            height = 0

        Animation(height=height, d=0.3, t='out_quart').start(self.root.ids.advanced_record_settings)

    def toggle_info_panel(self):
        '''Callback from UI to toggle info panel visibility'''
        self.show_info = not self.show_info
        if self.show_info:
            width = 300
        else:
            width = 0

        Animation(width=width, d=0.3, t='out_quart').start(self.root.ids.info_panel)

    def toggle_play_pause(self):
        '''Callback from UI to toggle play state of video'''
        self.play = not self.play

    def step_next_frame(self):
        '''Callback from UI to process only next frame'''
        if self.play:
            self.toggle_play_pause()

        self.next_frame = True

    def step_previous_frame(self):
        '''Callback from UI to process only previous frame'''
        if self.play:
            self.toggle_play_pause()

        index = int(self.capture.get(cv2.CAP_PROP_POS_FRAMES)) - 2
        if index > 0:
            self.capture.set(cv2.CAP_PROP_POS_FRAMES, index)

        self.previous_frame = True

    def snap(self):
        '''Callback from UI to quick save of last frame'''
        if self.last_frame is not None:
            index = int(self.capture.get(cv2.CAP_PROP_POS_FRAMES))
            cv2.imwrite('./Frame_' + str(index) + '.jpg', self.last_frame)

    def slider_on_touch_down(self):
        '''Callback from UI on slider touch down'''
        if self.play:
            self.toggle_play_pause()

    def slider_on_touch_up(self, instance):
        '''Callback from UI on slider release'''
        if not self.play:
            # Prevent access to self.capture from another thread
            sleep(1)

            self.frame_index = instance.value
            self.capture.set(cv2.CAP_PROP_POS_FRAMES, self.frame_index)
            self.toggle_play_pause()

    def slider_set_min_max(self, max_value, min_value=0):
        '''Updates sliders bounds'''
        self.root.ids.slider.min = min_value
        self.root.ids.slider.max = max_value

    def start_recording(self):
        '''Method that starts the video recording'''
        self.recording_state = True
        self.recording_pause = False
        self.record_logger = Logger()
        fourcc = cv2.VideoWriter_fourcc(*'{}'.format(self.output_fourcc))
        self.writer = cv2.VideoWriter(self.output_path, fourcc, self.output_fps,
                                      (self.output_width, self.output_height))

    def stop_recording(self):
        '''Method to save output video file'''
        self.recording_state = False
        self.recording_pause = False
        self.writer.release()

        if self.save_record_log and self.record_logger.log:
                header = '{}, FPS: {}, Shape: {}x{}'.format(self.output_path, self.output_fps,
                                                            self.output_width, self.output_height)
                self.record_logger.add_header(header)
                self.record_logger.save_log(os.path.splitext(self.output_path)[0] + '_log.txt')

        popup_text = 'Video file written into: {}'.format(self.output_path)
        self.show_popup('File written', popup_text, 'OK')

    def show_popup(self, title, text, button_text):
        '''Shows info popup with set text'''
        self.popup_title = title
        self.popup_text = text
        self.popup_button_text = button_text

        self.root.ids.text_bl.popup.open()

    def single_image(self, input_image):
        '''Method to detect hands in single frame'''
        img, boxes, _, hands, names = self.hand_tracker.predict(input_image)

        for box, hand, name in zip(boxes, hands, names):
            self.logger.add_line(' '.join(map(str, (box[0].numpy(), box[1].numpy(),
                                                    box[2].numpy(), box[3].numpy(),
                                                    hand.name, name))))

        self.output_frames.append(img)

    def video_feed(self):
        '''Method to detect hands in video feed'''
        record_frame_index = 0
        while not self.stop_threads:
            start_time = time()
            if self.play and self.capture is not None:
                if self.skip_frames != 0 and self.input_type == InputTypes.video:
                    index = int(self.capture.get(cv2.CAP_PROP_POS_FRAMES)) + self.skip_frames
                    self.capture.set(cv2.CAP_PROP_POS_FRAMES, index)

                ret, frame = self.capture.read()
                if ret:
                    img, boxes, _, hands, names = self.hand_tracker.predict(frame)
                    self.output_frames.append(img)

                    if self.input_type == InputTypes.video:
                        self.frame_index += (1 + self.skip_frames)

                        for box, hand, name in zip(boxes, hands, names):
                            stamp = get_timestamp(self.frame_index, self.input_fps)
                            stamp = '{:02d}:{:02d}:{:02d}'.format(stamp[0], stamp[1], stamp[2])
                            self.logger.add_record(self.frame_index, False, stamp, box[0].numpy(),
                                                   box[1].numpy(), box[2].numpy(), box[3].numpy(),
                                                   hand.name, name)
                    if self.recording_state:
                        for box, hand, name in zip(boxes, hands, names):
                            stamp = get_timestamp(record_frame_index, self.output_fps)
                            stamp = '{:02d}:{:02d}:{:02d}'.format(stamp[0], stamp[1], stamp[2])
                            self.record_logger.add_record(record_frame_index, False, stamp,
                                                          box[0].numpy(), box[1].numpy(),
                                                          box[2].numpy(), box[3].numpy(),
                                                          hand.name, name)
                        record_frame_index += 1
                else:
                    self.play = False
                    if self.recording_state:
                        self.stop_recording()
            elif self.next_frame or self.previous_frame:
                ret, frame = self.capture.read()
                if ret:
                    img, boxes, _, hands, names = self.hand_tracker.predict(frame)
                    self.output_frames.append(img)

                    if self.input_type == InputTypes.video:
                        if self.next_frame:
                            self.frame_index += 1
                        else:
                            self.frame_index -= 1

                        for box, hand, name in zip(boxes, hands, names):
                            stamp = get_timestamp(self.frame_index, self.input_fps)
                            stamp = '{:02d}:{:02d}:{:02d}'.format(stamp[0], stamp[1], stamp[2])
                            self.logger.add_record(self.frame_index, False, stamp, box[0].numpy(),
                                                   box[1].numpy(), box[2].numpy(), box[3].numpy(),
                                                   hand.name, name)

                    if self.recording_state:
                        for box, hand, name in zip(boxes, hands, names):
                            stamp = get_timestamp(record_frame_index, self.output_fps)
                            stamp = '{:02d}:{:02d}:{:02d}'.format(stamp[0], stamp[1], stamp[2])
                            self.record_logger.add_record(record_frame_index, False, stamp,
                                                          box[0].numpy(), box[1].numpy(),
                                                          box[2].numpy(), box[3].numpy(),
                                                          hand.name, name)
                        record_frame_index += 1
                else:
                    self.play = False
                    if self.recording_state:
                        self.stop_recording()

                self.next_frame = False
                self.previous_frame = False
            else:
                sleep(0.5)
            end_time = time()

            if not self.recording_state and record_frame_index > 0:
                record_frame_index = 0

            if self.frame_target != 0:
                if end_time - start_time < 1 / float(self.frame_target):
                    sleep(1 / float(self.frame_target) - (end_time - start_time))

    def video_thread_stop(self):
        '''Sends stop signal to video thread and waits until it stops'''
        if self.video_thread is not None and self.video_thread.isAlive():
            self.stop_threads = True
            self.video_thread.join()
            self.stop_threads = False

    def update_image(self, *args):
        '''Method periodically called to check for outputed frames'''
        if self.output_frames:
            self.last_frame = self.output_frames.pop(0)
            if self.last_frame is not None:
                self.root.ids.snap_button.disabled = False

            texture = cv_image_to_kivy_texture(self.last_frame)
            self.root.ids.output_image.texture = texture

            if self.recording_state and not self.recording_pause and self.writer is not None:
                resized = cv2.resize(self.last_frame, (self.output_width, self.output_height))
                self.writer.write(resized)

            self.show_save_image_button = True if self.input_type == InputTypes.image else False

if __name__ == "__main__":
    MainApp().run()
