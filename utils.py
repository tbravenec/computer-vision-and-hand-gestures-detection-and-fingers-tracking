import math
import sys

import cv2
import torch

def load_module(name, path):
    import importlib.util

    spec = importlib.util.spec_from_file_location(name, path)
    mod = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(mod)
    sys.modules['MainModel'] = mod

    return mod


def line_length(point_a, point_b):
    '''Calculates distance between two points'''
    return math.sqrt(math.pow(point_a[0] - point_b[0], 2) + math.pow(point_a[1] - point_b[1], 2))


def line_angle(point_a, point_b):
    '''Calculates an angle of a line defined by two points in radians'''
    return math.atan2(point_a[1] - point_b[1], point_a[0] - point_b[0])


def line_angle_degrees(point_a, point_b):
    '''Calculates an angle of a line defined by two points in degrees'''
    return math.degrees(line_angle(point_a, point_b))


def draw_label(img, text, x, y, up=True, scale=1, background=(255, 0, 0), color=(255, 255, 255)):
    '''Draws text with background into image'''
    font = cv2.FONT_HERSHEY_SIMPLEX
    scale = 1

    size, _ = cv2.getTextSize(text, font, scale, 1)

    if up:
        img = cv2.rectangle(img, (x, y), (x + size[0], y - size[1]), background, -1)

        img = cv2.putText(img, text, (x, y), font, scale, color, 1)
    else:
        img = cv2.rectangle(img, (x, y), (x + size[0], y + size[1]), background, -1)

        img = cv2.putText(img, text, (x, y + size[1]), font, scale, color, 1)

    return img


def check_cuda():
    '''Checks if CUDA is currentlz available'''
    return torch.cuda.is_available()


def cv_image_to_kivy_texture(img, color_format='bgr', data_format='ubyte'):
    from kivy.graphics.texture import Texture

    '''Converts opencv image to kivy texture'''
    buffer = cv2.flip(img, 0).tostring()
    texture = Texture.create(size=(img.shape[1], img.shape[0]), colorfmt=color_format)
    texture.blit_buffer(buffer, colorfmt=color_format, bufferfmt=data_format)

    return texture


def cv_count_cameras(max_cameras):
    '''Counts capture devices connected to system'''
    cameras = 0
    for i in range(max_cameras):
        cap = cv2.VideoCapture(i)

        try:
            ret, img = cap.read()
            if not ret:
                cap.release()
                continue
            else:
                cameras += 1
        except :
            cap.release()
            continue

    return cameras


def get_timestamp(frame_index, framerate):
    '''Calculates time from the frame index and video framerate'''
    hours = int(frame_index / framerate / 60 / 60)
    minutes = int(frame_index / framerate / 60)
    seconds = int(frame_index / framerate % 60)

    return (hours, minutes, seconds)