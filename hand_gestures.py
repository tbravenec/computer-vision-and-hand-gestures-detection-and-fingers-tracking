'''Module with functions for hand keypoints to gesture matching'''
import json
import os

from enums.finger_bends import FingerBends
from enums.finger_spread import FingerSpread
from enums.hands import Hands
from enums.hand_directions import HandDirections
from enums.thumb_positions import ThumbPositions
from enums.thumb_tip_positions import ThumbTipPositions
from enums.thumb_directions import ThumbDirections
from utils import line_length, line_angle_degrees


def finger_bends_from_keypoints(keypoints, dictionary=None):
    '''Detects bend state of fingers from keypoints'''
    if dictionary is None:
        dictionary = dict()

    finger_bends = dict()

    for i, finger in enumerate(['index', 'middle', 'ring', 'little']):
        step = i * 4

        if (keypoints[5 + step] is not None and keypoints[6 + step] is not None and
            keypoints[7 + step] is not None and keypoints[8 + step] is not None):

            dist = line_length(keypoints[5 + step], keypoints[8 + step])
            middle = line_length(keypoints[6 + step], keypoints[7 + step])
            length = (line_length(keypoints[5 + step], keypoints[6 + step])
                      + line_length(keypoints[6 + step], keypoints[7 + step])
                      + line_length(keypoints[7 + step], keypoints[8 + step]))

            if middle >= dist * 0.7:
                finger_bends[finger] = FingerBends.fully_bent
            elif length * 0.9 < dist:
                finger_bends[finger] = FingerBends.straight
            else:
                finger_bends[finger] = FingerBends.partly_bent

        else:
            finger_bends[finger] = FingerBends.none

    # Thumb has one link less than other fingers, needs different calculations
    if keypoints[2] is not None and keypoints[3] is not None and keypoints[4] is not None:
        dist = line_length(keypoints[2], keypoints[4])
        length = line_length(keypoints[2], keypoints[3]) + line_length(keypoints[3], keypoints[4])

        if length * 0.9 < dist:
            finger_bends['thumb'] = FingerBends.straight
        else:
            finger_bends['thumb'] = FingerBends.partly_bent

    else:
        finger_bends['thumb'] = FingerBends.none

    dictionary['finger_bends'] = finger_bends

    return dictionary


def finger_spreads_from_keypoints(keypoints, dictionary=None):
    '''Detect if fingers are spread or not'''
    if dictionary is None:
        dictionary = dict()

    finger_spreads = dict()

    for i, fingers in enumerate(['index-middle', 'middle-ring']):
        step = i * 4

        if (keypoints[5 + step] is not None and keypoints[8 + step] is not None and
            keypoints[9 + step] is not None and keypoints[12 + step] is not None):

            base_dist = line_length(keypoints[5 + step], keypoints[9 + step])
            tip_dist = line_length(keypoints[8 + step], keypoints[12 + step])

            if base_dist * 1.5 < tip_dist:
                finger_spreads[fingers] = FingerSpread.far
            else:
                finger_spreads[fingers] = FingerSpread.close

        else:
            finger_spreads[fingers] = FingerSpread.none

    # Tip of little finger is much lower, distance is compared between
    # keypoints 15 and 20, instead of 16 and 20
    if (keypoints[13] is not None and keypoints[15] is not None and
        keypoints[17] is not None and keypoints[20] is not None):

        base_dist = line_length(keypoints[13], keypoints[17])
        tip_dist = line_length(keypoints[15], keypoints[20])

        if base_dist * 1.5 < tip_dist:
            finger_spreads['ring-little'] = FingerSpread.far
        else:
            finger_spreads['ring-little'] = FingerSpread.close

    else:
        finger_spreads['ring-little'] = FingerSpread.none

    dictionary['finger_spreads'] = finger_spreads

    return dictionary


def thumb_position_from_keypoints(keypoints, dictionary=None):
    '''Detects position of thumb'''
    if dictionary is None:
        dictionary = dict()

    if (keypoints[3] is not None and keypoints[4] is not None and
        keypoints[5] is not None and keypoints[9] is not None):

        index_dist = line_length(keypoints[4], keypoints[5])
        middle_dist = line_length(keypoints[4], keypoints[9])
        base_index_dist = line_length(keypoints[3], keypoints[5])

        if middle_dist < index_dist * 0.8:
            dictionary['thumb-position'] = ThumbPositions.over
        elif index_dist < base_index_dist and index_dist < middle_dist:
            dictionary['thumb-position'] = ThumbPositions.close
        else:
            dictionary['thumb-position'] = ThumbPositions.far

    else:
        dictionary['thumb-position'] = ThumbPositions.none

    return dictionary


def thumb_tip_position_from_keypoints(keypoints, dictionary=None):
    '''Detects position of the tip of the thumb'''
    if dictionary is None:
        dictionary = dict()

    thumb_tip = list()

    for i, tip in enumerate(ThumbTipPositions, -1):
        if tip in (ThumbTipPositions.none, ThumbTipPositions.any):
            continue

        step = i * 4

        if (keypoints[3] is not None and keypoints[4] is not None and
            keypoints[7 + step] is not None and keypoints[8 + step] is not None):

            tip_dist = line_length(keypoints[4], keypoints[8 + step])
            thumb_dist = line_length(keypoints[3], keypoints[4])
            finger_dist = line_length(keypoints[7 + step], keypoints[8 + step])

            if tip_dist < thumb_dist or tip_dist < finger_dist:
                thumb_tip.append(tip)

    if not thumb_tip:
        thumb_tip.append(ThumbTipPositions.none)

    dictionary['thumb-tip'] = thumb_tip

    return dictionary


def thumb_direction_from_keypoints(keypoints, dictionary=None):
    '''Detects direction of thumb'''
    if dictionary is None:
        dictionary = dict()

    if (keypoints[2] is not None and keypoints[4] is not None):
        angle = line_angle_degrees(keypoints[2], keypoints[4])

        if -45 <= angle < 45:
            dictionary['thumb-direction'] = ThumbDirections.right
        elif 45 <= angle < 135:
            dictionary['thumb-direction'] = ThumbDirections.up
        elif -135 <= angle < -45:
            dictionary['thumb-direction'] = ThumbDirections.down
        else:
            dictionary['thumb-direction'] = ThumbDirections.left
    else:
        dictionary['thumb-direction'] = ThumbDirections.none

    return dictionary


def hand_direction_from_keypoints(keypoints, dictionary=None):
    '''Detects direction of hand'''
    if dictionary is None:
        dictionary = dict()

    if (keypoints[0] is not None and keypoints[5] is not None and keypoints[9] is not None and
        keypoints[13] is not None and keypoints[17] is not None):

        x = int((keypoints[5][0] + keypoints[9][0] + keypoints[13][0] + keypoints[17][0]) / 4)
        y = int((keypoints[5][1] + keypoints[9][1] + keypoints[13][1] + keypoints[17][1]) / 4)

        angle = line_angle_degrees(keypoints[0], (x, y))

        if -45 <= angle < 45:
            dictionary['hand-direction'] = HandDirections.right
        elif 45 <= angle < 135:
            dictionary['hand-direction'] = HandDirections.up
        elif -135 <= angle < -45:
            dictionary['hand-direction'] = HandDirections.down
        else:
            dictionary['hand-direction'] = HandDirections.left
    else:
        dictionary['hand-direction'] = HandDirections.none

    return dictionary


def pose_from_keypoints(keypoints):
    '''Returns state of detected fingers from keypoints'''
    pose = finger_bends_from_keypoints(keypoints)
    pose = finger_spreads_from_keypoints(keypoints, pose)
    pose = thumb_position_from_keypoints(keypoints, pose)
    pose = thumb_tip_position_from_keypoints(keypoints, pose)
    pose = thumb_direction_from_keypoints(keypoints, pose)
    pose = hand_direction_from_keypoints(keypoints, pose)

    return pose


def load_gestures(directory):
    '''Parse json files in directory and return gesture list'''
    gestures = list()

    for filename in os.listdir(directory):
        if not filename.endswith(".json"):
            continue

        with open(os.path.join(directory, filename), 'r') as file:
            json_dict = json.load(file)

        gesture = dict()

        for key in json_dict.keys():
            if key in 'name':
                gesture[key] = json_dict[key]
            elif key in 'hand':
                gesture[key] = list()
                for hand in Hands:
                    if hand.name in json_dict[key]:
                        gesture[key].append(hand)
            elif key in 'finger_bends':
                gesture[key] = dict()
                for finger in json_dict[key]:
                    gesture[key][finger] = list()
                    for finger_bend in FingerBends:
                        if finger_bend.name in json_dict[key][finger]:
                            gesture[key][finger].append(finger_bend)
            elif key in 'finger_spreads':
                gesture[key] = dict()
                for fingers in json_dict[key]:
                    gesture[key][fingers] = list()
                    for finger_spread in FingerSpread:
                        if finger_spread.name in json_dict[key][fingers]:
                            gesture[key][fingers].append(finger_spread)
            elif key in 'thumb-position':
                gesture[key] = list()
                for thumb_position in ThumbPositions:
                    if thumb_position.name in json_dict[key]:
                        gesture[key].append(thumb_position)
            elif key in 'thumb-tip':
                gesture[key] = list()
                for thumb_tip_position in ThumbTipPositions:
                    if thumb_tip_position.name in json_dict[key]:
                        gesture[key].append(thumb_tip_position)
            elif key in 'thumb-direction':
                gesture[key] = list()
                for thumb_direction in ThumbDirections:
                    if thumb_direction.name in json_dict[key]:
                        gesture[key].append(thumb_direction)
            elif key in 'hand-direction':
                gesture[key] = list()
                for hand_direction in HandDirections:
                    if hand_direction.name in json_dict[key]:
                        gesture[key].append(hand_direction)

        gestures.append(gesture)

    return gestures


def match_pose_to_gesture(pose, hand, gesture_list):
    '''Matches pose to gesture from gesture list and returns it's name'''
    for gesture in gesture_list.copy():
        for key in gesture.keys():
            if gesture not in gesture_list:
                break
            elif key in 'hand':
                if Hands.any in gesture[key]:
                    continue
                else:
                    if hand not in gesture[key]:
                        gesture_list.remove(gesture)
                        break
            elif key in 'finger_bends':
                for finger in gesture[key]:
                    if FingerBends.any in gesture[key][finger]:
                        continue
                    else:
                        if pose[key][finger] not in gesture[key][finger]:
                            gesture_list.remove(gesture)
                            break
            elif key in 'finger_spreads':
                for fingers in gesture[key]:
                    if FingerSpread.any in gesture[key][fingers]:
                        continue
                    else:
                        if pose[key][fingers] not in gesture[key][fingers]:
                            gesture_list.remove(gesture)
                            break
            elif key in 'thumb-position':
                if ThumbPositions.any in gesture[key]:
                    continue
                else:
                    if pose[key] not in gesture[key]:
                        gesture_list.remove(gesture)
                        break
            elif key in 'thumb-tip':
                if ThumbTipPositions.any in gesture[key]:
                    continue
                else:
                    for finger in pose[key]:
                        if finger not in gesture[key]:
                            gesture_list.remove(gesture)
                            break
            elif key in 'thumb-direction':
                if ThumbDirections.any in gesture[key]:
                    continue
                else:
                    if pose[key] not in gesture[key]:
                        gesture_list.remove(gesture)
                        break
            elif key in 'hand-direction':
                if HandDirections.any in gesture[key]:
                    continue
                else:
                    if pose[key] not in gesture[key]:
                        gesture_list.remove(gesture)
                        break

    return gesture_list[0]['name'] if gesture_list else 'Unknown'


def gesture_from_keypoints(keypoints, hand, gestures):
    '''Matches keypoints to gesture'''
    pose = pose_from_keypoints(keypoints)
    gesture = match_pose_to_gesture(pose, hand, gestures.copy())

    return gesture
