from enum import Enum

class FingerBends(Enum):
    """Finger state enum"""
    none = 0
    straight = 1
    partly_bent = 2
    fully_bent = 3
    any = 4
