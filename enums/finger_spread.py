from enum import Enum

class FingerSpread(Enum):
    """Finger state enum"""
    none = 0
    close = 1
    far = 2
    any = 3
