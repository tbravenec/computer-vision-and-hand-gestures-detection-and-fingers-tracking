from enum import Enum

class HandDirections(Enum):
    '''Hand direction state enum'''
    none = 0
    up = 1
    down = 2
    left = 3
    right = 4
    any = 5

