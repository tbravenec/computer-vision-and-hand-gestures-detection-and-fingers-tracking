from enum import Enum

class ThumbTipPositions(Enum):
    """Finger state enum"""
    none = 0
    index = 1
    middle = 2
    ring = 3
    little = 4
    any = 5
