from enum import Enum

class InputTypes(Enum):
    '''Input types enum'''
    none = 0
    image = 1
    video = 2
    camera = 3
