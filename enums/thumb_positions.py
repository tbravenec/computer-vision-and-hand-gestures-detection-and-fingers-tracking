from enum import Enum

class ThumbPositions(Enum):
    """Finger state enum"""
    none = 0
    over = 1
    close = 2
    far = 3
    any = 4
