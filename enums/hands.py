from enum import Enum

class Hands(Enum):
    '''Possible hands'''
    unknown = 0
    right = 1
    left = 2
    any = 3
