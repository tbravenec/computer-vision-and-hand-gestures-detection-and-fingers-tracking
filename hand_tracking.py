'''Class for HandTracking'''
import os

import cv2
import torch

from hand_keypoints import detect_right_left, draw_skeleton, get_hand_offset, keypoints_offset
from utils import draw_label, load_module
from darknet.darknet import Darknet
from darknet.utils import do_detect, load_class_names, plot_boxes_cv2
from darknet.utils import get_cropped, bounding_box_padding, log_bbox_padding
from hand_gestures import gesture_from_keypoints, load_gestures
from enums.hands import Hands
from get_models import get_detection_model, get_keypoint_model

class HandTracking():
    '''Class containing configuration and functions for hand tracking'''
    draw_boxes = True
    draw_gesture_label = True
    draw_skeleton = True
    draw_skeleton_points = True
    use_cuda = False
    use_detector = False
    use_keypoints = False
    use_gesture_matching = False
    half_precision = False

    net_detect = None
    net_keypoints = None

    gesture_list = list()

    cfg_detector = None
    weights_detector = None
    weights_keypoints = None

    def __init__(self, cfg_detector, classes, weights_detector, cfg_keypoints, weights_keypoints):
        if cfg_detector is not None and weights_detector is not None:
            if os.path.exists(cfg_detector) and os.path.exists(weights_detector):
                self.net_detect = Darknet(cfg_detector, self.use_cuda)
                self.net_detect.load_weights(weights_detector)
                self.cfg_detector = cfg_detector
                self.weights_detector = weights_detector

                if classes is not None:
                    self.class_names = load_class_names(classes)
                else:
                    self.class_names = None

                self.use_detector = True
            else:
                self.use_detector = False
        else:
            self.use_detector = False

        if cfg_keypoints is not None and weights_keypoints is not None:
            if os.path.exists(cfg_keypoints) and os.path.exists(weights_keypoints):
                load_module('MainModel', cfg_keypoints)
                self.net_keypoints = torch.load(weights_keypoints)
                self.weights_keypoints = weights_keypoints

                self.use_keypoints = True
            else:
                self.use_keypoints = False
        else:
            self.use_keypoints = False

    def half(self):
        '''Casts both detection models to half precision'''
        if self.net_detect is not None:
            self.net_detect.half()

        if self.net_keypoints is not None:
            self.net_keypoints.half()

        self.half_precision = True

    def float(self):
        '''Casts both detection models to single precision'''
        if self.net_detect is not None:
            self.net_detect = Darknet(self.cfg_detector, self.use_cuda)
            self.net_detect.load_weights(self.weights_detector)

        if self.net_keypoints is not None:
            self.net_keypoints = torch.load(self.weights_keypoints)

        if self.use_cuda:
            self.cuda()

        self.half_precision = False

    def cuda(self):
        '''Moves both detection models to GPU'''
        if torch.cuda.is_available():
            if self.net_detect is not None:
                self.net_detect.cuda()

            if self.net_keypoints is not None:
                self.net_keypoints.cuda()

            self.use_cuda = True
        else:
            self.use_cuda = False

        return self.use_cuda

    def cpu(self):
        '''Moves both detection models to CPU'''
        if self.net_detect is not None:
            self.net_detect.cpu()

        if self.net_keypoints is not None:
            self.net_keypoints.cpu()

        self.use_cuda = False

        torch.cuda.empty_cache()

        return self.use_cuda

    def set_use_detector(self, value=True):
        '''Set usage of hand detector'''
        self.use_detector = value

        return self.use_detector

    def set_use_keypoints(self, value=True):
        '''Set usage of keypoint detector'''
        self.use_keypoints = value

        return self.use_keypoints

    def set_draw_boxes(self, value=True):
        '''Set drawing bounding boxes'''
        self.draw_boxes = value

        return self.draw_boxes

    def set_draw_skeleton(self, value=True):
        '''Set drawing hand skeleton'''
        self.draw_skeleton = value

        return self.draw_skeleton

    def set_draw_skeleton_points(self, value=True):
        '''Set draw skeleton points'''
        self.draw_skeleton_points = value

        return self.draw_skeleton_points

    def set_use_gesture_matching(self, value=True):
        '''Set usage of gesture matching'''
        if self.gesture_list:
            self.use_gesture_matching = value

        return self.use_gesture_matching

    def load_gestures(self, folder, use_gesture_matching=True):
        '''Load gestures'''
        self.gesture_list = load_gestures(folder)

        self.set_use_gesture_matching(use_gesture_matching)

        return self.use_gesture_matching

    def predict(self, img):
        '''Run predictions on input frame'''
        box = torch.FloatTensor([0.5, 0.5, 1, 1, 0, 0, 0])
        bboxes = list()
        keypoint_list = list()
        hands = list()
        names = list()

        if self.use_detector:
            rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            sized = cv2.resize(rgb, (self.net_detect.width, self.net_detect.height))
            bboxes = do_detect(self.net_detect, sized, 0.5, 0.4,
                               self.use_cuda, self.half_precision)

            if self.draw_boxes:
                draw_img = plot_boxes_cv2(img.copy(), bboxes, None, self.class_names)
            else:
                draw_img = img

            padded_bboxes = bounding_box_padding(bboxes, log_bbox_padding(img.shape))

            # Stop detection if use keypoints is False
            if not self.use_keypoints:
                keypoint_list = [[None for j in range(22)] for i in range(len(bboxes))]
                hands = [Hands.unknown for i in range(len(bboxes))]
                names = ['unknown' for i in range(len(bboxes))]
                return draw_img, bboxes, keypoint_list, hands, names

            cropped = get_cropped(img, padded_bboxes)
            for box, padded_box, cropped_img in zip(bboxes, padded_bboxes, cropped):
                keypoints, hand = detect_right_left(self.net_keypoints, cropped_img,
                                                    self.use_cuda, self.half_precision)

                x, y = get_hand_offset(img, padded_box)
                keypoints = keypoints_offset(keypoints, x, y)
                keypoint_list.append(keypoints)
                hands.append(hand)

                if self.draw_skeleton:
                    draw_img = draw_skeleton(draw_img, keypoints, self.draw_skeleton_points)
                else:
                    draw_img = img

                if self.use_gesture_matching:
                    name = gesture_from_keypoints(keypoints, hand, self.gesture_list)
                    names.append(name)

                    x, y = get_hand_offset(img, box)
                    draw_label(draw_img, name, x, y, True)
                else:
                    names.append('unknown')
        elif self.use_keypoints:
            # Keypoint detection running without hand detector
            keypoints, hand = detect_right_left(self.net_keypoints, img,
                                                self.use_cuda, self.half_precision)
            keypoint_list.append(keypoints)

            if keypoints.count(None) > 10:
                hands.append(Hands.unknown)
            else:
                hands.append(hand)

            if self.draw_skeleton:
                draw_img = draw_skeleton(img, keypoints, self.draw_skeleton_points)
            else:
                draw_img = img

            if self.use_gesture_matching:
                name = gesture_from_keypoints(keypoints, hand, self.gesture_list)
                names.append(name)

                draw_label(draw_img, name, 0, 0, False)
            else:
                names.append('unknown')
            bboxes.append([box[i] for i in range(7)])
        else:
            keypoint_list.append([None for x in range(22)])
            bboxes.append([box[i] for i in range(7)])
            hands.append(Hands.unknown)
            names.append('unknown')

            return img, bboxes, keypoint_list, hands, names

        return draw_img, bboxes, keypoint_list, hands, names

def main(use_cuda=True, half_precision=False):
    '''Function ran if module is Main program'''
    cfgfile_detector = 'cfg/yolov2.cfg'
    weights_detector = get_detection_model('weights')
    cfg_keypoints = 'cfg/keypoints.py'
    weights_keypoints = get_keypoint_model('weights')

    hand_tracker = HandTracking(cfgfile_detector, None, weights_detector,
                                cfg_keypoints, weights_keypoints)

    hand_tracker.load_gestures('gestures')

    if half_precision:
        hand_tracker.half()

    if use_cuda:
        hand_tracker.cuda()

    cap = cv2.VideoCapture(0)
    key = cv2.waitKey(1)

    if not cap.isOpened():
        print("Unable to open camera")
        exit(-1)

    while True:
        res, img = cap.read()
        if res:
            draw_img, bboxes, keypoints, hands, names = hand_tracker.predict(img)
            cv2.imshow('Window', draw_img)
            key = cv2.waitKey(1)
            if key == ord('q'):
                break
        else:
            print("Unable to read image")
            exit(-1)


if __name__ == '__main__':
    main(torch.cuda.is_available(), True)
